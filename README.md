# uniapp-ql-demo
#### :dizzy_face: 首先确认分支是否在开发分支，master分支版本较为滞后
#### 介绍
1. 基于uni-app设计的前端页面模板
2. 每个页面单独样式(无全局样式)
3. 测试机使用为安卓，IOS没使用

#### 项目演示
1. 列表筛选
![列表筛选](https://images.gitee.com/uploads/images/2020/0218/174730_af321127_1613286.gif "search.gif")

#### 软件架构
1. 图标采用uni-app官方提供的icon库[链接导航](https://ext.dcloud.net.cn/plugin?id=28)

#### 使用说明

1.  **本项目是自己学习练习demo，求大佬勿喷**
2.  码云更新快一点  **[码云地址](https://gitee.com/stoneql517/uniapp-ql-demo)**
3.  [uniapp插件库地址](https://ext.dcloud.net.cn/plugin?id=1304)

